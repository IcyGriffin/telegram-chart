var express = require('express');
const path = require('path');
var app = express();
var port = process.env.PORT || 5000;

app
  .use(express.static(path.join(__dirname, 'dist')));

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
