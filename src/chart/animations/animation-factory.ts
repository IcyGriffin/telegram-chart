import { StripLineAnimation } from './strip-line-animation';
import { Chart } from '../chart';

interface AnimationsMap {
    [id: string]: StripLineAnimation;
}

export class AnimationFactory {

    private animations: AnimationsMap = {};

    by(id: string, chart: Chart, duration: number): StripLineAnimation {

        if (!this.animations.hasOwnProperty(id)) {
            this.animations[id] = new StripLineAnimation(chart, duration);
        }

        return this.animations[id];
    }

    findRemoved(existedIds: string[]): StripLineAnimation[] {
        return Object.keys(this.animations).filter(key => {
            return !existedIds.some(existedKey => existedKey === key);
        }).map(key => {
            return this.animations[key];
        });
    }
}
