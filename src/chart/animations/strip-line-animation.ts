import { StripLine } from '../../engine/models/strip-line.model';
import { Axis } from '../../engine/models/axis.model';
import { HelperService } from '../helper.service';
import { Point } from '../../engine/models/point.model';
import { Chart } from '../chart';

interface AxisMap {
    xAxis: Axis;
    yAxis: Axis;
}

export class StripLineAnimation {
    private startPointY: number;
    private finishPointY: number;

    private startPoints: Point[];
    private finishPoints: Point[];

    private startTime: number;
    private finishTime: number;

    private line: StripLine;

    private isRemoved = false;
    private isRemovingInProgress = false;

    constructor(private chart: Chart, private duration: number) {}

    animate(line: StripLine, axises: AxisMap, timeline: number): StripLine {
        if (this.startPointY === null || this.startPointY === undefined) {
            this.initialDraw(line, axises, timeline);
        }

        this.isRemoved = false;
        this.isRemovingInProgress = false;

        if (this.shouldRecalculateFinishLine(line, axises)) {

            this.startPoints.forEach((point, index) => {
                const finishPointY = this.finishPoints[index].y;
                const startPointY = this.startPoints[index].y;

                const speed = (finishPointY - startPointY) / this.duration;
                const deltaTime = timeline - this.startTime;
                const translateY = finishPointY - (startPointY + (speed * deltaTime));

                point.y = finishPointY - translateY;
            });

            this.startTime = timeline;
            this.finishTime = timeline + this.duration;
            this.setFinishPointY(line, axises.yAxis);
            this.finishPoints = this.calclulatePoints(line, axises);
        }

        if (!this.shouldRecalculateCurrentLine(timeline)) {
            this.setStartPointY(line, axises.yAxis);
            this.startPoints = this.finishPoints;
        }

        let points: Point[] = [];
        if (this.shouldRecalculateCurrentLine(timeline)) {
            const deltaTime = timeline - this.startTime;
            points = this.calculateAnimatedPoints(line, axises, deltaTime);
            this.chart.requestRedraw();
        } else {
            points = this.calclulatePoints(line, axises);
        }

        return {
            ...line,
            points: points
        };
    }

    animateRemove(axises: AxisMap, timeline: number): StripLine {
        if (this.isRemoved) {
            this.isRemovingInProgress = false;
            return;
        }

        if (!this.isRemovingInProgress) {
            this.startPoints.forEach((point, index) => {
                const finishPointY = this.finishPoints[index].y;
                const startPointY = this.startPoints[index].y;

                const speed = (finishPointY - startPointY) / this.duration;
                const deltaTime = timeline - this.startTime;
                const translateY = finishPointY - (startPointY + (speed * deltaTime));

                point.y = finishPointY - translateY;
            });

            this.startTime = timeline;
            this.finishTime = timeline + this.duration;
            this.setFinishPointY(this.line, axises.yAxis);
            this.finishPoints = this.calclulatePoints(this.line, axises);
        }

        this.isRemovingInProgress = true;

        if (!this.shouldRecalculateCurrentLine(timeline)) {
            this.setStartPointY(this.line, axises.yAxis);
            this.startPoints = this.finishPoints;
            this.isRemoved = true;
        }

        const deltaTime = timeline - this.startTime;
        const points = this.calculateAnimatedPoints(this.line, axises, deltaTime);

        const colorSpeed = (0 - this.line.color.a) / this.duration;
        const color = {
            ...this.line.color,
            a: this.line.color.a + (colorSpeed * deltaTime)
        };

        this.chart.requestRedraw();

        return {
            ...this.line,
            color: color,
            points: points
        };
    }

    private initialDraw(line: StripLine, axises: AxisMap, timeline: number) {
        this.setStartPointY(line, axises.yAxis);
        this.setFinishPointY(line, axises.yAxis);
        this.line = line;

        this.startPoints = this.calclulatePoints(line, axises);
        this.finishPoints = this.calclulatePoints(line, axises);
        this.startTime = timeline;
    }

    private shouldRecalculateFinishLine(newLine: StripLine, axises: AxisMap): boolean {
        // super otimization =( if first point changed - animation is required
        const firstNewPointY = HelperService.mapDomainToScale(
            newLine.points[0].y,
            axises.yAxis.domain,
            axises.yAxis.scale
        );

        return firstNewPointY !== this.finishPointY;
    }

    private shouldRecalculateCurrentLine(timeline: number) {
        return timeline <= this.finishTime;
    }

    private calclulatePoints(line: StripLine, axises: AxisMap): Point[] {
        return line.points.map<Point>(point => {
            return {
                x: HelperService.mapDomainToScale(
                    point.x,
                    axises.xAxis.domain,
                    axises.xAxis.scale
                ),
                y: HelperService.mapDomainToScale(
                    point.y,
                    axises.yAxis.domain,
                    axises.yAxis.scale
                )
            };
        });
    }

    private setFinishPointY(line: StripLine, axis: Axis) {
        this.finishPointY = this.calculatePoint(line, axis);
    }

    private setStartPointY(line: StripLine, axis: Axis) {
        this.startPointY = this.calculatePoint(line, axis);
    }

    private calculatePoint(line: StripLine, axis: Axis) {
        return HelperService.mapDomainToScale(
            line.points[0].y,
            axis.domain,
            axis.scale
        );
    }

    private calculateAnimatedPoints(line: StripLine, axises: AxisMap, deltaTime: number) {
        return line.points.map<Point>((point, index) => {
            const finishPointY = this.finishPoints[index].y;
            const startPointY = this.startPoints[index].y;

            const speed = (finishPointY - startPointY) / this.duration;
            const translateY = speed * deltaTime;

            return {
                x: HelperService.mapDomainToScale(
                    point.x,
                    axises.xAxis.domain,
                    axises.xAxis.scale
                ),
                y: this.startPoints[index].y + translateY,
            };
        });
    }
}