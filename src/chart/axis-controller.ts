import { Limit } from '../engine/models/limit.model';
import { Line } from '../engine/models/line.model';
import { Axis } from '../engine/models/axis.model';
import { HelperService } from './helper.service';
import { TransparentColor } from '../engine/models/transparent-color.model';
import { DateFormattingService } from './date-formatting.service';
import { Theme } from './models/theme.model';
import { ColorService } from './color.service';

export class AxisController {

    lines: Line[] = [];

    private readonly axisYMarkNumber = 6;
    private readonly markerOffset = 20;
    private readonly xValuesRecalculateFactor = 2;
    private readonly defaultXMarkNumber = 6;
    private readonly xMarkSpacing = 100;
    private readonly minimalXValueStep = 1000 * 60 * 60 * 24; // one day
    private color: TransparentColor;

    private markColor: string;

    private xAxis: Axis;
    private yAxis: Axis;
    private fullAxis: Axis;

    // portion of yAxis.domain
    private limit: Limit = {
        min: 0,
        max: 250,
    };

    private readonly attributeName = 'marker-id';

    private yMarkersContainer: HTMLDivElement;
    private xMarkersContainer: HTMLDivElement;
    private yValues: number[] = [];
    private xValues: number[] = [];

    private xValueToElementMap: Record<number, HTMLElement> = {};

    setMin(min: number) {
        this.limit.min = min;
    }

    setMax(max: number) {
        this.limit.max = max;
    }

    setAxises(xAxis: Axis, yAxis: Axis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    setFullXAxis(xAxis: Axis) {
        this.fullAxis = xAxis;
    }

    recalculate() {
        this.yValues = this.recalculateYValues();
        this.recalculateLines();
        this.recalculateYMarkers();
        this.recalculateXMarkers();
    }

    setTheme(theme: Theme) {
        this.markColor = theme.markColor;
        this.color = {
            ...ColorService.hexToRgb(theme.yAxisLineColor),
            a: 1
        };

        this.updateTheme();
    }

    recalculateXValues() {
        if (this.xValues.length > 1 && !this.isRecalculationRequired(this.xValues, this.xAxis)) {
            return;
        }

        let step = (this.xAxis.domain.max - this.xAxis.domain.min) / (this.getXAxisMarkNumber(this.xAxis) - 1);
        step = Math.floor(step / this.minimalXValueStep) * this.minimalXValueStep;
        if (step === 0) {
            step = this.minimalXValueStep;
        }
        const fittedValues: number[] = [];
        let currentXValue = this.fullAxis.domain.min;
        while (currentXValue <= this.fullAxis.domain.max) {
            fittedValues.push(currentXValue);
            currentXValue += step;
        }

        const xMarkersIds = fittedValues.map((xValue) => {
            return xValue;
        });

        this.xValues = fittedValues;
        this.recreateXMarkers(this.xMarkersContainer, xMarkersIds);
    }

    createMarkers(container: HTMLDivElement) {
        this.yMarkersContainer = this.createMarkerContainer(container, 'y-axis-markers', this.axisYMarkNumber);
        this.xMarkersContainer = this.createMarkerContainer(container, 'x-axis-markers', 0);
    }

    private getXAxisMarkNumber(axis: Axis) {
        return Math.floor(axis.scale.max / this.xMarkSpacing) || this.defaultXMarkNumber;
    }

    private isRecalculationRequired(xValues: number[], xAxis: Axis): boolean {

        const firstValue = xValues[0];
        const scaleFirstValue = HelperService.mapDomainToScale(firstValue, xAxis.domain, xAxis.scale);

        const nextValue = xValues[1];
        const scaleNextValue = HelperService.mapDomainToScale(nextValue, xAxis.domain, xAxis.scale);

        const compareTo = Math.round((xAxis.scale.max - xAxis.scale.min) / (this.getXAxisMarkNumber(this.xAxis) - 1));
        const diff = Math.round(scaleNextValue - scaleFirstValue);

        return (diff < (compareTo / this.xValuesRecalculateFactor)) || (diff > (compareTo * this.xValuesRecalculateFactor));
    }

    private recreateXMarkers(container: HTMLDivElement, markerIds: number[]) {
        const children = <HTMLElement[]>[].concat(...container.children as any);
        const elementsToHide = children.filter(element => {
            const id = element.getAttribute(this.attributeName);
            return !markerIds.some(markerId => markerId === parseInt(id, 10));
        });

        const elementsToAdd = markerIds.filter(markerId => {
            return !children.some(element => parseInt(element.getAttribute(this.attributeName), 10) === markerId);
        });

        elementsToHide.forEach(element => {
            element.style.opacity = '0';
        });

        elementsToAdd.forEach(markerId => {
            const text = this.getXMarkerText(markerId);
            this.xValueToElementMap[markerId] = this.createMarker(container, text, markerId);
        });
    }

    private createMarkerContainer(container: HTMLDivElement, className: string, amount: number): HTMLDivElement {
        const markerContainer = document.createElement('div');
        markerContainer.className = className;
        markerContainer.style.pointerEvents = 'none';
        markerContainer.style.userSelect = 'none';
        markerContainer.style.color = this.markColor;
        for (let index = 0; index < amount; index++) {
            this.createMarker(markerContainer);
        }

        return container.appendChild(markerContainer);
    }

    private createMarker(container: HTMLDivElement, text: string = '', markerId: number = 0) {
        const marker = document.createElement('div');
        marker.setAttribute(this.attributeName, markerId as any);
        marker.className = 'marker';
        marker.style.position = 'absolute';
        marker.style.whiteSpace = 'nowrap';
        marker.style.opacity = '0';
        marker.style.transition = 'opacity 200ms linear';
        marker.textContent = text;
        return container.appendChild(marker);
    }

    private recalculateLines() {
        this.lines = this.yValues.map(value => {
            return this.getLine(value);
        });
    }

    private recalculateYMarkers() {
        this.yValues.forEach((value, index) => {
            const element = this.yMarkersContainer.children.item(index) as HTMLDivElement;
            const markerPosition = HelperService.mapDomainToScale(value, this.yAxis.domain, this.yAxis.scale)
                + this.markerOffset;
            element.style.transform = `translate(0, -${ markerPosition }px)`;
            element.style.opacity = '1';
            element.textContent = Math.round(value).toString();
        });
    }

    private recalculateXMarkers() {
        this.xValues.forEach((value) => {
            const element = this.xValueToElementMap[value];

            const startOfTheDay = new Date(value).setHours(0, 0, 0, 0);
            const markerPosition = HelperService.mapDomainToScale(startOfTheDay, this.xAxis.domain, this.xAxis.scale);
            element.style.transform = `translate(${ markerPosition }px, -${ this.yAxis.scale.min }px)`;
            element.style.opacity = '1';
        });
    }

    private recalculateYValues(): number[] {
        return this.recalculateValues(this.limit.min, this.limit.max, this.axisYMarkNumber);
    }

    private recalculateValues(min: number, max: number, amount: number) {
        const result: number[] = [min];

        const valuesLeft = amount - 2; // do not include min and max, added manually
        const step = (max - min) / (valuesLeft + 1);
        let current = min;
        for (let index = 0; index < valuesLeft; index++) {
            current += step;
            result.push(current);
        }

        result.push(max);
        return result;
    }

    private updateTheme() {
        this.yMarkersContainer.style.color = this.markColor;
        this.xMarkersContainer.style.color = this.markColor;
    }

    private getXMarkerText(value: number) {
        return DateFormattingService.format(new Date(value));
    }

    private getLine(value: number): Line {
        return {
            from: {
                x: this.xAxis.scale.min,
                y: HelperService.mapDomainToScale(value, this.yAxis.domain, this.yAxis.scale)
            },
            to: {
                x: this.xAxis.scale.max,
                y: HelperService.mapDomainToScale(value, this.yAxis.domain, this.yAxis.scale)
            },
            color: this.color
        };
    }
}
