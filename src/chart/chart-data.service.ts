import { ChartData, ChartColumnData } from './models/chart-data.model';
import { Serie } from './models/serie.model';
import { SeriePoint } from './models/serie-model';

export class ChartDataService {
    static getSeries(chartData: ChartData): Serie[] {
        const serieIds = Object.keys(chartData.types)
            .filter(key => chartData.types[key] === 'line');

        const xSerieIds = Object.keys(chartData.types)
            .filter(key => chartData.types[key] === 'x');
        const xSerieId = xSerieIds[0];

        return serieIds.map<Serie>(serieId => {
            return {
                id: serieId,
                name: chartData.names[serieId],
                color: chartData.colors[serieId],
                data: ChartDataService.getSeriePoints(chartData, serieId, xSerieId)
            };
        });
    }

    static getSeriePoints(chartData: ChartData, serieId: string, xSerieId: string): SeriePoint[] {
        const xValues = ChartDataService.getColumn(chartData, xSerieId);
        const yValues = ChartDataService.getColumn(chartData, serieId);

        const seriePoints: SeriePoint[] = [];
        for (let index = 1; index < xValues.length; index++) {
            seriePoints.push({
                x: new Date(xValues[index]),
                y: yValues[index] as number
            });
        }

        return seriePoints;
    }

    static getColumn(chartData: ChartData, serieId: string): ChartColumnData {
        return chartData.columns.filter(column => column[0] === serieId)[0];
    }
}