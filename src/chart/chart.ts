import { ChartData } from './models/chart-data.model';
import { Engine } from '../engine/engine';
import { StripLine } from '../engine/models/strip-line.model';
import { Serie } from './models/serie.model';
import { ChartDataService } from './chart-data.service';
import { SeriesService } from './series.service';
import { Axis } from '../engine/models/axis.model';
import { LineChartRenderer } from '../engine/renderers/line-chart/line-chart-renderer';
import { Size } from '../engine/models/size.model';
import { HiderRenderer } from '../engine/renderers/hider/hider-renderer';
import { Selector } from './selector';
import { Hider } from './models/hider.model';
import { SelectorBoxRenderer } from '../engine/renderers/selector-box/selector-box-renderer';
import { MousePosition } from './models/mouse-position.model';
import { InteractionState } from './models/Interaction-state.model';
import { UnselectedState } from './interaction-states/unselected-state';
import { ResizeState } from './interaction-states/resize-state';
import { MoveState } from './interaction-states/move-state';
import { LineRenderer } from '../engine/renderers/line/line-renderer';
import { AxisController } from './axis-controller';
import { CircleRenderer } from '../engine/renderers/circle/circle-renderer';
import { HiddenState } from './tooltip-states/hidden-state';
import { TooltipState } from './models/tooltip-state.model';
import { ShownState } from './tooltip-states/shown-state';
import { Theme } from './models/theme.model';
import { ColorService } from './color.service';
import { AnimationFactory } from './animations/animation-factory';

interface Storage {
    axises: {
        xAxis: Axis,
        yAxis: Axis
    };
}

interface SetDataOptions {
    isUpdate: boolean;
}

export class Chart {

    private chartData: ChartData;

    private container: HTMLElement;
    private canvas: HTMLCanvasElement;
    private markers: HTMLDivElement;
    private tooltip: HTMLDivElement;
    private tooltipHeader: HTMLDivElement;
    private tooltipValues: HTMLDivElement;

    private gl: WebGLRenderingContext;
    private engine: Engine;

    private resolution: Size;

    private lineChartRenderer: LineChartRenderer;
    private hiderRenderer: HiderRenderer;
    private selectorBoxRenderer: SelectorBoxRenderer;
    private lineRenderer: LineRenderer;
    private circleRenderer: CircleRenderer;

    private series: Serie[];

    private main: Storage = {
        axises: {
            xAxis: this.createDefaultAxis(),
            yAxis: this.createDefaultAxis()
        }
    };

    private chartPadding = {
        top: 20,
        bottom: 130,
        left: 5,
        right: 5
    };

    private backgroundColor: string;

    private tooltipBorderColor: string;
    private tooltipBackgroundColor: string;
    private tooltipHeaderColor: string;

    private cursors = [
        {
            checker: (mousePosition: MousePosition) => this.selector.isOverBorders(mousePosition),
            cursor: 'w-resize'
        },
        {
            checker: (mousePosition: MousePosition) => this.selector.isOverSelectorBox(mousePosition),
            cursor: 'move'
        }
    ];

    private selector = new Selector();
    private axisController = new AxisController();

    private interactionStates = {
        unselected: new UnselectedState(this),
        resize: new ResizeState(this),
        move: new MoveState(this)
    };

    private tooltipStates = {
        hidden: new HiddenState(this),
        shown: new ShownState(this),
    };

    private interactionState: InteractionState = this.interactionStates.unselected;
    private tooltipState: TooltipState = this.tooltipStates.hidden;

    private xSelectorMinWidth = 1000 * 60 * 60 * 24 * 3; // three days

    private seriesLines: StripLine[] = [];

    private currentTimeline: number;

    private mainStripAnimationFactory = new AnimationFactory();
    private selectorStripAnimationFactory = new AnimationFactory();
    private animationDuration = 200;
    private redrawRequested = false;
    private resizeRedrawTime = 500;
    private previousResizeTime = 0;

    constructor(container: HTMLElement, chartData: ChartData) {
        this.container = container;
        this.container.style.position = 'relative';
        this.container.style.overflow = 'hidden';
        this.canvas = this.createCanvas(this.container);
        this.markers = this.createMarkers(this.container);
        this.tooltip = this.createTooltip(this.container);
        this.axisController.createMarkers(this.markers);
        this.gl = this.canvas.getContext('webgl', {
            premultipliedAlpha: false
        }) || this.canvas.getContext( 'experimental-webgl', {
            premultipliedAlpha: false
        } );

        this.engine = new Engine(this.gl);
        this.lineChartRenderer = new LineChartRenderer(this.gl, this.engine);
        this.hiderRenderer = new HiderRenderer(this.gl, this.engine);
        this.selectorBoxRenderer = new SelectorBoxRenderer(this.gl, this.engine);
        this.lineRenderer = new LineRenderer(this.gl, this.engine);
        this.circleRenderer = new CircleRenderer(this.gl, this.engine);

        this.axisController.setAxises(this.main.axises.xAxis, this.main.axises.yAxis);
        this.axisController.setFullXAxis(this.selector.axises.xAxis);

        this.canvas.addEventListener('mousemove', (event: MouseEvent) => this.onCanvasMouseMove(event));
        this.canvas.addEventListener('touchmove', (event: TouchEvent) => this.onCanvasMouseMove(event));

        document.addEventListener('mousemove', (event: MouseEvent) => this.onMouseMove(event));
        document.addEventListener('touchmove', (event: TouchEvent) => this.onMouseMove(event));

        document.addEventListener('mousedown', (event: MouseEvent) => this.onMouseDown(event));
        document.addEventListener('touchstart', (event: TouchEvent) => this.onMouseDown(event));

        document.addEventListener('mouseup', (event: MouseEvent) => this.onMouseUp(event));
        document.addEventListener('touchend', (event: TouchEvent) => this.onMouseUp(event));
        window.addEventListener('resize', () => this.resize());

        this.chartData = chartData;
        this.setData(chartData, { isUpdate: false });

        this.resize();
        this.requestRedraw();
    }

    showSeries(ids: string[]) {
        if (ids.length === 0) {
            return;
        }

        const xTypes = Object.keys(this.chartData.types).reduce((result, serieId) => {
            const type = this.chartData.types[serieId];
            if (type === 'x' || ids.some(id => id === serieId)) {
                result[serieId] = type;
            }
            return result;
        }, {} as any);

        const newChartData: ChartData = {
            ...this.chartData,
            types: xTypes
        };
        this.setData(newChartData, { isUpdate: true });
        this.requestRedraw();
    }

    setTheme(theme: Theme) {
        this.backgroundColor = theme.primaryColor;

        this.tooltipBackgroundColor = theme.primaryColor;
        this.tooltipBorderColor = theme.tooltipBorderColor;
        this.tooltipHeaderColor = theme.contrastColor;

        this.updateTooltipTheme();
        this.selector.setTheme(theme);
        this.axisController.setTheme(theme);

        Object.keys(this.tooltipStates).forEach((key: 'shown' | 'hidden') => {
            this.tooltipStates[key].setTheme(theme);
        });
    }

    getSelector(): Selector {
        return this.selector;
    }

    getMainAxises(): Storage {
        return this.main;
    }

    setInteractionState(state: InteractionState, options?: any) {
        this.interactionState.stop();
        this.interactionState = state;
        this.interactionState.start(options);
    }

    getInteractionStates() {
        return this.interactionStates;
    }

    setTooltipState(state: TooltipState, options?: any) {
        this.tooltipState.stop();
        this.tooltipState = state;
        this.tooltipState.start(options);
    }

    getTooltipStates() {
        return this.tooltipStates;
    }

    getTooltipMarkup() {
        return {
            container: this.tooltip,
            header: this.tooltipHeader,
            values: this.tooltipValues
        };
    }

    requestRedraw() {
        if (this.redrawRequested) {
            return;
        }

        requestAnimationFrame((newTimeline) => {
            this.redrawRequested = false;
            this.draw(newTimeline);
        });

        this.redrawRequested = true;
    }

    setSelection(min: number, max: number, isRecalculateXValues: boolean) {
        this.setSelectionMin(min, isRecalculateXValues);
        this.setSelectionMax(max, isRecalculateXValues);
    }

    setSelectionMin(value: number, isRecalculateXValues = true) {
        if (value < this.selector.axises.xAxis.domain.min) {
            value = this.selector.axises.xAxis.domain.min;
        }

        if (this.selector.toXDomain - value <= this.xSelectorMinWidth) {
            value = this.selector.toXDomain - this.xSelectorMinWidth;
        }

        this.main.axises.xAxis.domain.min = value;

        this.selector.setSelectionMin(value);
        this.selector.recalculate();
        if (isRecalculateXValues) {
            this.axisController.recalculateXValues();
        }

        this.requestRedraw();
    }

    setSelectionMax(value: number, isRecalculateXValues = true) {
        if (value > this.selector.axises.xAxis.domain.max) {
            value = this.selector.axises.xAxis.domain.max;
        }

        if (value - this.selector.fromXDomain <= this.xSelectorMinWidth) {
            value = this.selector.fromXDomain + this.xSelectorMinWidth;
        }

        this.main.axises.xAxis.domain.max = value;

        this.selector.setSelectionMax(value);
        this.selector.recalculate();
        if (isRecalculateXValues) {
            this.axisController.recalculateXValues();
        }

        this.requestRedraw();
    }

    adjustYAxisDomain() {
        const maxYValue = SeriesService.getMaxYValueInXDomain(this.series, this.main.axises.xAxis.domain);
        this.main.axises.yAxis.domain.max = maxYValue;
        this.adjustYAxisLimits(this.main.axises.yAxis.domain.min, maxYValue);
    }

    adjustYAxisLimits(minValue: number, maxValue: number) {
        this.axisController.setMin(minValue);
        this.axisController.setMax(maxValue);
    }

    getSeries(): Serie[] {
        return this.series;
    }

    private draw(timeline: number) {
        this.currentTimeline = timeline;

        this.clear();
        this.drawAxis();
        this.drawSeries();
        this.drawTooltip();
        this.drawSelector();
    }

    private drawAxis() {
        this.axisController.recalculate();
        this.lineRenderer.draw({
            lines: this.axisController.lines,
            resolution: this.resolution
        });
    }

    private drawSeries() {
        this.seriesLines.forEach((seriesLine) => {
            const animationService = this.mainStripAnimationFactory.by(
                this.getMainStripAnimationId(seriesLine),
                this,
                this.animationDuration
            );

            this.lineChartRenderer.draw({
                line: animationService.animate(seriesLine, this.main.axises, this.currentTimeline),
                axises: {
                    x: this.main.axises.xAxis,
                    y: this.main.axises.yAxis,
                },
                resolution: this.resolution
            });
        });

        const existedIds = this.seriesLines.map(line => this.getMainStripAnimationId(line));
        this.mainStripAnimationFactory.findRemoved(existedIds).forEach(animationService => {
            const line = animationService.animateRemove(this.main.axises, this.currentTimeline);
            if (!line) {
                return;
            }

            this.lineChartRenderer.draw({
                line: line,
                axises: {
                    x: this.main.axises.xAxis,
                    y: this.main.axises.yAxis,
                },
                resolution: this.resolution
            });
        });
    }

    private drawTooltip() {
        this.tooltipState.draw(this.circleRenderer, this.lineRenderer, this.resolution);
    }

    private drawSelector() {
        this.drawMiniSeries();
        this.drawHiders();
        this.drawSelectorBox();
    }

    private drawMiniSeries() {
        this.seriesLines.forEach(seriesLine => {
            const animationService = this.selectorStripAnimationFactory.by(
                this.getSelectorStripAnimationId(seriesLine),
                this,
                this.animationDuration
            );

            this.lineChartRenderer.draw({
                line: animationService.animate(seriesLine, this.selector.axises, this.currentTimeline),
                axises: {
                    x: this.selector.axises.xAxis,
                    y: this.selector.axises.yAxis,
                },
                resolution: this.resolution
            });
        });
    }

    private drawHiders() {
        [this.selector.hiders.left, this.selector.hiders.right].forEach((hider: Hider) => {
            this.hiderRenderer.draw({
                rectangle: {
                    ...hider,
                    color: this.selector.hiderColor
                },
                resolution: this.resolution
            });
        });
    }

    private drawSelectorBox() {
        this.selectorBoxRenderer.draw({
            rectangle: {
                ...this.selector.selectorBox,
                color: this.selector.selectorBoxColor,
            },
            resolution: this.resolution
        });
    }

    private setData(chartData: ChartData, { isUpdate }: SetDataOptions) {
        this.series = ChartDataService.getSeries(chartData);
        this.seriesLines = this.series.map(serie => SeriesService.toStripLine(serie));

        this.selector.axises.xAxis.domain = SeriesService.getDomain(this.series, (point => point.x.getTime()));
        this.selector.axises.yAxis.domain = {
            min: 0,
            max: SeriesService.getDomain(this.series, (point => point.y)).max
        };

        if (!isUpdate) {

            this.main.axises.xAxis.domain = {
                min: this.selector.axises.xAxis.domain.min,
                max: this.selector.axises.xAxis.domain.max
            };

            this.main.axises.yAxis.domain = {
                min: this.selector.axises.yAxis.domain.min,
                max: this.selector.axises.yAxis.domain.max
            };

            const recalculateXValues = true;
            this.setSelection(
                this.selector.axises.xAxis.domain.min,
                this.selector.axises.xAxis.domain.max,
                recalculateXValues
            );
        }

        this.adjustYAxisDomain();
    }

    private clear() {
        this.engine.clear(ColorService.hexToRgb(this.backgroundColor));
    }

    private resize() {
        const { clientWidth, clientHeight} = this.canvas;
        if (this.canvas.width === clientWidth && this.canvas.height === clientHeight) {
           return;
        }

        this.resolution = {
            width: clientWidth,
            height: clientHeight
        };

        this.setAxisScales();
        this.setSelectorScales();
        this.axisController.recalculateXValues();

        const now = performance.now();
        const deltaTime = now - this.previousResizeTime;

        if (this.previousResizeTime === 0 || deltaTime >= this.resizeRedrawTime) {
            this.previousResizeTime = now;

            this.canvas.width = this.resolution.width;
            this.canvas.height = this.resolution.height;

            this.engine.setSize({
                width: this.resolution.width,
                height: this.resolution.height
            });
        }

        requestAnimationFrame(() => this.resize());
        this.requestRedraw();
    }

    private setAxisScales() {
        this.main.axises.xAxis.scale = {
            min: this.chartPadding.left,
            max: this.resolution.width - this.chartPadding.right
        };

        this.main.axises.yAxis.scale = {
            min: this.chartPadding.bottom,
            max: this.resolution.height - this.chartPadding.top
        };
    }

    private setSelectorScales() {
        this.selector.axises.xAxis.scale = Object.create(this.main.axises.xAxis.scale);
        this.selector.axises.yAxis.scale = {
            min: 0,
            max: 100
        };

        this.selector.recalculate();
    }

    private onCanvasMouseMove(event: MouseEvent | TouchEvent) {
        const mousePosition = event instanceof MouseEvent
            ? this.getMousePosition(event)
            : this.getTouchPosition(event);

        this.setCursor(mousePosition);
    }

    private onMouseMove(event: MouseEvent | TouchEvent) {
        const mousePosition = event instanceof MouseEvent
            ? this.getMousePosition(event)
            : this.getTouchPosition(event);

        this.interactionState.onMouseMove(mousePosition);
        this.tooltipState.onMouseMove(mousePosition);
    }

    private onMouseDown(event: MouseEvent | TouchEvent) {
        const mousePosition = event instanceof MouseEvent
            ? this.getMousePosition(event)
            : this.getTouchPosition(event);

        this.interactionState.onMouseDown(mousePosition);
    }

    private onMouseUp(event: MouseEvent | TouchEvent) {
        const mousePosition = event instanceof MouseEvent
            ? this.getMousePosition(event)
            : this.getTouchPosition(event);

        this.interactionState.onMouseUp(mousePosition);
    }

    private setCursor(mousePosition: MousePosition) {
        let isDefault = true;
        this.cursors.forEach((cursorHandler) => {
            if (isDefault && cursorHandler.checker(mousePosition)) {
                this.canvas.style.cursor = cursorHandler.cursor;
                isDefault = false;
            }
        });

        if (isDefault) {
            this.canvas.style.cursor = 'default';
        }
    }

    private getMousePosition(event: MouseEvent): MousePosition {
        return {
            mouseX: event.pageX - this.container.offsetLeft,
            mouseY: this.canvas.clientHeight - Math.max(event.pageY - this.container.offsetTop, 0),
            radiusX: 0,
            radiusY: 0,
        };
    }

    private getTouchPosition(event: TouchEvent): MousePosition {
        const { pageX, pageY, radiusX, radiusY } = event.changedTouches[0];
        return {
            mouseX: pageX - this.container.offsetLeft,
            mouseY: this.canvas.clientHeight - Math.max(pageY - this.container.offsetTop, 0),
            radiusX: radiusX >= 20 ? radiusX : 20,
            radiusY: radiusY >= 20 ? radiusY : 20,
        };
    }

    private createCanvas(container: HTMLElement): HTMLCanvasElement {
        const canvas = document.createElement('canvas');
        canvas.style.width = '100%';
        canvas.style.height = '100%';
        return container.appendChild(canvas);
    }

    private createMarkers(container: HTMLElement): HTMLDivElement {
        const markers = document.createElement('div');
        markers.style.position = 'absolute';
        return container.appendChild(markers);
    }

    private createTooltip(container: HTMLElement): HTMLDivElement {
        const tooltipContainer = document.createElement('div');
        tooltipContainer.className = 'tooltip-container';
        tooltipContainer.style.position = 'absolute';
        tooltipContainer.style.display = 'flex';
        tooltipContainer.style.flexDirection = 'column';
        tooltipContainer.style.pointerEvents = 'none';
        tooltipContainer.style.userSelect = 'none';
        tooltipContainer.style.fontWeight = 'bold';
        tooltipContainer.style.borderWidth = '1px';
        tooltipContainer.style.borderStyle = 'solid';
        tooltipContainer.style.borderRadius = '8px';

        this.tooltipHeader = this.createTooltipHeader(tooltipContainer);
        this.tooltipValues = this.createTooltipValues(tooltipContainer);

        return container.appendChild(tooltipContainer);
    }

    private updateTooltipTheme() {
        this.tooltip.style.backgroundColor = this.tooltipBackgroundColor;
        this.tooltip.style.borderColor = this.tooltipBorderColor;
        this.tooltip.style.boxShadow = `1px 3px 5px -3px ${ this.tooltipBorderColor }`;
        this.tooltipHeader.style.color = this.tooltipHeaderColor;
        this.requestRedraw();
    }

    private createTooltipHeader(container: HTMLElement): HTMLDivElement {
        const tooltipHeader = document.createElement('div');
        tooltipHeader.className = 'tooltip-header';
        tooltipHeader.style.display = 'flex';
        tooltipHeader.style.alignItems = 'center';
        tooltipHeader.style.justifyContent = 'center';
        tooltipHeader.style.padding = '10px';
        tooltipHeader.style.paddingBottom = '0';
        return container.appendChild(tooltipHeader);
    }

    private createTooltipValues(container: HTMLElement): HTMLDivElement {
        const tooltipValues = document.createElement('div');
        tooltipValues.className = 'tooltip-values';
        tooltipValues.style.display = 'flex';
        tooltipValues.style.alignItems = 'center';
        tooltipValues.style.justifyContent = 'center';
        return container.appendChild(tooltipValues);
    }

    private getMainStripAnimationId(line: StripLine) {
        return `main-strip-line-${ line.id }`;
    }

    private getSelectorStripAnimationId(line: StripLine) {
        return `selector-strip-line-${ line.id }`;
    }

    private createDefaultAxis(): Axis {
        return {
            domain: {
                min: 0,
                max: 1
            },
            scale: {
                min: 0,
                max: 1
            }
        };
    }
}
