import { Color } from '../engine/models/color.model';

export class ColorService {
    public static hexToRgb(hex: string): Color {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function(m, r, g, b) {
            return r + r + g + g + b + b;
        });

        const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: ColorService.normalize(parseInt(result[1], 16)),
            g: ColorService.normalize(parseInt(result[2], 16)),
            b: ColorService.normalize(parseInt(result[3], 16))
        } : null;
    }

    private static normalize(value: number) {
        return value / 255;
    }
}
