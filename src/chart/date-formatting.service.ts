export class DateFormattingService {
    // lightweight date formatter
    static monthNames = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec',
    ];
    static format(date: Date): string {
        return `${ DateFormattingService.monthNames[date.getMonth()] } ${ date.getDate() }`;
    }
}
