import { Limit } from '../engine/models/limit.model';

export class HelperService {

    static mapDomainToScale(value: number, domain: Limit, scale: Limit) {
        return (value - domain.min) / (domain.max - domain.min) * (scale.max - scale.min) + scale.min;
    }

    static mapScaleToDomain(value: number, domain: Limit, scale: Limit) {
        return (value - scale.min) / (scale.max - scale.min) * (domain.max - domain.min) + domain.min;
    }
}
