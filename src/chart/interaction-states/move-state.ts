import { InteractionState } from '../models/Interaction-state.model';
import { Chart } from '../chart';
import { MousePosition } from '../models/mouse-position.model';
import { HelperService } from '../helper.service';

export class MoveState implements InteractionState {

    private selector = this.chart.getSelector();

    private startMousePosition: MousePosition;
    private startMinScale: number;
    private startMaxScale: number;

    constructor(private chart: Chart) {}

    onMouseMove(mousePosition: MousePosition) {
        let minValue = HelperService.mapScaleToDomain(
            this.startMinScale + (mousePosition.mouseX - this.startMousePosition.mouseX),
            this.selector.axises.xAxis.domain,
            this.selector.axises.xAxis.scale
        );
        let maxValue = HelperService.mapScaleToDomain(
            this.startMaxScale + (mousePosition.mouseX - this.startMousePosition.mouseX),
            this.selector.axises.xAxis.domain,
            this.selector.axises.xAxis.scale
        );

        if (minValue < this.selector.axises.xAxis.domain.min) {
            maxValue = this.selector.axises.xAxis.domain.min + (maxValue - minValue);
            minValue = this.selector.axises.xAxis.domain.min;
        }

        if (maxValue > this.selector.axises.xAxis.domain.max) {
            minValue = this.selector.axises.xAxis.domain.max - (maxValue - minValue);
            maxValue = this.selector.axises.xAxis.domain.max;
        }

        const recalculateXValues = false;
        this.chart.setSelection(minValue, maxValue, recalculateXValues);

        this.chart.adjustYAxisDomain();
    }

    onMouseDown(_mousePosition: MousePosition) {}

    onMouseUp(_mousePosition: MousePosition) {
        this.chart.setInteractionState(this.chart.getInteractionStates().unselected);
    }

    start(startMousePosition: MousePosition) {
        this.startMousePosition = startMousePosition;
        this.startMinScale = HelperService.mapDomainToScale(
            this.selector.fromXDomain,
            this.selector.axises.xAxis.domain,
            this.selector.axises.xAxis.scale
        );
        this.startMaxScale = HelperService.mapDomainToScale(
            this.selector.toXDomain,
            this.selector.axises.xAxis.domain,
            this.selector.axises.xAxis.scale
        );
    }

    stop() {
        this.startMousePosition = null;
        this.startMinScale = null;
        this.startMaxScale = null;
    }
}
