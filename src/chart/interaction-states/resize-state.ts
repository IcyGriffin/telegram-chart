import { InteractionState } from '../models/Interaction-state.model';
import { Chart } from '../chart';
import { MousePosition } from '../models/mouse-position.model';
import { HelperService } from '../helper.service';

export class ResizeState implements InteractionState {

    private selector = this.chart.getSelector();

    private border: string;

    constructor(private chart: Chart) {}

    onMouseMove(mousePosition: MousePosition) {
        const newValue = HelperService.mapScaleToDomain(
            mousePosition.mouseX,
            this.selector.axises.xAxis.domain,
            this.selector.axises.xAxis.scale
        );

        if (this.border === 'left') {
            this.chart.setSelectionMin(newValue);
            this.chart.adjustYAxisDomain();
        }

        if (this.border === 'right') {
            this.chart.setSelectionMax(newValue);
            this.chart.adjustYAxisDomain();
        }
    }

    onMouseDown(_mousePosition: MousePosition) {}

    onMouseUp(_mousePosition: MousePosition) {
        this.chart.setInteractionState(this.chart.getInteractionStates().unselected);
    }

    start(border: string) {
        this.border = border;
    }

    stop() {
        this.border = null;
    }
}
