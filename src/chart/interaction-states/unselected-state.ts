import { Chart } from '../chart';
import { InteractionState } from '../models/Interaction-state.model';
import { MousePosition } from '../models/mouse-position.model';

export class UnselectedState implements InteractionState {

    private selector = this.chart.getSelector();

    constructor(private chart: Chart) {
    }

    onMouseMove(_mousePosition: MousePosition) {}

    onMouseDown(mousePosition: MousePosition) {
        if (this.selector.isOverLeftBorder(mousePosition)) {
            this.chart.setInteractionState(this.chart.getInteractionStates().resize, 'left');
            return;
        }

        if (this.selector.isOverRightBorder(mousePosition)) {
            this.chart.setInteractionState(this.chart.getInteractionStates().resize, 'right');
            return;
        }

        if (this.selector.isOverSelectorBox(mousePosition)) {
            this.chart.setInteractionState(this.chart.getInteractionStates().move, mousePosition);
            return;
        }
    }

    onMouseUp(_mousePosition: MousePosition) {}

    start() {}

    stop() {}
}
