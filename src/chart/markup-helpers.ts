export function createContainer() {
    return document.createElement('div');
}

export function createChartTitle(chartName: string) {
    const chartTitle = document.createElement('h2');
    chartTitle.textContent = chartName;
    return chartTitle;
}

export function createChartContainer() {
    const chartContainer = document.createElement('div');
    chartContainer.className = 'chart-container chart';
    return chartContainer;
}

export function createChartLegend() {
    const legendContainer = document.createElement('div');
    legendContainer.className = 'legend-container';
    return legendContainer;
}
