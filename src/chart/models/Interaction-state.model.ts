import { MousePosition } from './mouse-position.model';

export interface InteractionState {

    onMouseDown(mousePosition: MousePosition): void;
    onMouseUp(mousePosition: MousePosition): void;
    onMouseMove(mousePosition: MousePosition): void;
    start(options: any): void;
    stop(): void;
}
