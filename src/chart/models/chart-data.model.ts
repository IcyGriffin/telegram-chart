export type ChartColumnData = [string, ...number[]];

export interface ChartData {
    colors: {
        [serieId: string]: string;
    };
    columns: ChartColumnData[];
    names: {
        [serieId: string]: string;
    };
    types: {
        [serieId: string]: 'line' | 'x';
    };
}
