import { Point } from '../../engine/models/point.model';
import { Size } from '../../engine/models/size.model';

export interface Hider {
    from: Point;
    size: Size;
}
