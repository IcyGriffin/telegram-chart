export interface MousePosition {
    mouseX: number;
    mouseY: number;
    radiusX: number;
    radiusY: number;
}
