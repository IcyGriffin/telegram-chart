export interface SeriePoint {
    x: Date;
    y: number;
}
