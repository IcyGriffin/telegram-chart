import { SeriePoint } from './serie-model';

export interface Serie {
    id: string;
    name: string;
    color: string;
    data: SeriePoint[];
}
