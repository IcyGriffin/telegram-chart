export interface Theme {
    primaryColor: string;
    contrastColor: string;
    tooltipBorderColor: string;
    tooltipLineColor: string;
    markColor: string;
    yAxisLineColor: string;
    selectorBoxColor: string;
    selectorHiderColor: string;
}
