import { MousePosition } from './mouse-position.model';
import { CircleRenderer } from '../../engine/renderers/circle/circle-renderer';
import { Size } from '../../engine/models/size.model';
import { LineRenderer } from '../../engine/renderers/line/line-renderer';
import { Theme } from './theme.model';

export interface TooltipState {

    onMouseMove(mousePosition: MousePosition): void;
    draw(renderer: CircleRenderer, lineRenderer: LineRenderer, resolution: Size): void;
    start(options: any): void;
    stop(): void;
    setTheme(theme: Theme): void;
}
