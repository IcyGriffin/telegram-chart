import { Axis } from '../engine/models/axis.model';
import { Hider } from './models/hider.model';
import { SelectorBox } from './models/selector-box.model';
import { TransparentColor } from '../engine/models/transparent-color.model';
import { MousePosition } from './models/mouse-position.model';
import { HelperService } from './helper.service';
import { Theme } from './models/theme.model';
import { ColorService } from './color.service';

interface HiderStorage {
    left: Hider;
    right: Hider;
}

export class Selector {

    public axises = {
        xAxis: this.createDefaultAxis(),
        yAxis: this.createDefaultAxis()
    };

    public hiderColor: TransparentColor;

    public selectorBoxColor: TransparentColor;

    public hiders: HiderStorage = {
        left: this.createDefaultHider(),
        right: this.createDefaultHider()
    };

    public selectorBox: SelectorBox = this.createDefaultSelectorBox();

    public fromXDomain: number;
    public toXDomain: number;

    private mousePositionDelta = 5;

    constructor() {}

    setSelectionMin(from: number) {
        this.fromXDomain = from;
    }

    setSelectionMax(to: number) {
        this.toXDomain = to;
    }

    recalculate() {
        this.recalculateHiders();
        this.recalculateSelectorBox();
    }

    setTheme(theme: Theme) {
        this.selectorBoxColor = {
            ...ColorService.hexToRgb(theme.selectorBoxColor),
            a: 0.85
        };

        this.hiderColor = {
            ...ColorService.hexToRgb(theme.selectorHiderColor),
            a: 0.7
        };
    }

    isOverBorders(mousePosition: MousePosition) {
        return this.isOverLeftBorder(mousePosition) || this.isOverRightBorder(mousePosition);
    }

    isOverLeftBorder(mousePosition: MousePosition) {
        return this.isOverSelector(mousePosition) && this._isOverLeftBorder(mousePosition);
    }

    isOverRightBorder(mousePosition: MousePosition) {
        return this.isOverSelector(mousePosition) && this._isOverRightBorder(mousePosition);
    }

    isOverSelectorBox(mousePosition: MousePosition) {
        if (this.isOverSelector(mousePosition)) {
            const borderLeftPosition = HelperService.mapDomainToScale(
                this.fromXDomain,
                this.axises.xAxis.domain,
                this.axises.xAxis.scale
            );

            const borderRightPosition = HelperService.mapDomainToScale(
                this.toXDomain,
                this.axises.xAxis.domain,
                this.axises.xAxis.scale
            );

            const delta = mousePosition.radiusX
            ? mousePosition.radiusX
            : this.mousePositionDelta;
            return mousePosition.mouseX > borderLeftPosition + delta
                && mousePosition.mouseX < borderRightPosition - delta;
        }

        return false;
    }

    private recalculateHiders() {
        const fromLeft = {
            x: this.axises.xAxis.scale.min,
            y: this.axises.yAxis.scale.min
        };
        this.hiders.left = {
            from: fromLeft,
            size: {
                width: HelperService.mapDomainToScale(
                    this.fromXDomain,
                    this.axises.xAxis.domain,
                    this.axises.xAxis.scale
                ) - fromLeft.x,
                height: this.axises.yAxis.scale.max,
            }
        };

        const fromRight = {
            x: HelperService.mapDomainToScale(
                this.toXDomain,
                this.axises.xAxis.domain,
                this.axises.xAxis.scale
            ),
            y: this.axises.yAxis.scale.min
        };
        this.hiders.right = {
            from: fromRight,
            size: {
                width: this.axises.xAxis.scale.max - fromRight.x,
                height: this.axises.yAxis.scale.max,
            }
        };
    }

    private recalculateSelectorBox() {
        const from = {
            x: HelperService.mapDomainToScale(
                this.fromXDomain,
                this.axises.xAxis.domain,
                this.axises.xAxis.scale
            ),
            y: this.axises.yAxis.scale.min
        };
        this.selectorBox = {
            from: from,
            size: {
                width: HelperService.mapDomainToScale(
                    this.toXDomain,
                    this.axises.xAxis.domain,
                    this.axises.xAxis.scale
                ) - from.x,
                height: this.axises.yAxis.scale.max,
            }
        };
    }

    private _isOverLeftBorder(mousePosition: MousePosition) {
        const borderPosition = HelperService.mapDomainToScale(
            this.fromXDomain,
            this.axises.xAxis.domain,
            this.axises.xAxis.scale
        );

        const delta = mousePosition.radiusX
            ? mousePosition.radiusX
            : this.mousePositionDelta;
        return mousePosition.mouseX >= borderPosition - delta
            && mousePosition.mouseX <= borderPosition + delta;
    }

    private _isOverRightBorder(mousePosition: MousePosition) {
        const borderPosition = HelperService.mapDomainToScale(
            this.toXDomain,
            this.axises.xAxis.domain,
            this.axises.xAxis.scale
        );

        const delta = mousePosition.radiusX
            ? mousePosition.radiusX
            : this.mousePositionDelta;
        return mousePosition.mouseX >= borderPosition - delta && mousePosition.mouseX <= borderPosition + delta;
    }

    private isOverSelector(mousePosition: MousePosition): boolean {
        const delta = mousePosition.radiusY;
        return mousePosition.mouseY >= this.axises.yAxis.scale.min - delta
            && mousePosition.mouseY <= this.axises.yAxis.scale.max + delta;
    }

    private createDefaultAxis(): Axis {
        return {
            domain: {
                min: 0,
                max: 1
            },
            scale: {
                min: 0,
                max: 1
            }
        };
    }

    private createDefaultHider(): Hider {
        return {
            from: {
                x: 0,
                y: 0
            },
            size: {
                width: 0,
                height: 0
            }
        };
    }

    private createDefaultSelectorBox(): SelectorBox {
        return {
            from: {
                x: 0,
                y: 0
            },
            size: {
                width: 0,
                height: 0
            }
        };
    }
}
