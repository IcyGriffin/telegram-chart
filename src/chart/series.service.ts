import { Serie } from './models/serie.model';
import { StripLine } from '../engine/models/strip-line.model';
import { Point } from '../engine/models/point.model';
import { Limit } from '../engine/models/limit.model';
import { SeriePoint } from './models/serie-model';
import { ColorService } from './color.service';

export class SeriesService {
    static toStripLine(serie: Serie): StripLine {
        return {
            id: serie.id,
            color: {
                ...ColorService.hexToRgb(serie.color),
                a: 1
            },
            points: serie.data.map<Point>(seriePoint => {
                return {
                    x: seriePoint.x.getTime(),
                    y: seriePoint.y
                };
            })
        };
    }

    static getDomain(series: Serie[], valueSelector: (point: SeriePoint) => number): Limit {
        const result: Limit = {
            min: Number.MAX_VALUE,
            max: Number.MIN_VALUE
        };

        series.forEach(serie => {
            serie.data.forEach(point => {
                const xValue = valueSelector(point);
                if (xValue < result.min) {
                    result.min = xValue;
                }

                if (xValue > result.max) {
                    result.max = xValue;
                }
            });
        });

        return result;
    }

    static getMaxYValueInXDomain(series: Serie[], xDomain: Limit): number {
        let maxValue = Number.MIN_VALUE;

        series.forEach(serie => {
            const indexes = serie.data.reduce<number[]>((result, point, index) => {
                const xValue = point.x.getTime();
                if (xValue >= xDomain.min && xValue <= xDomain.max) {
                    result.push(index);
                }

                return result;
            }, []);

            const beforeLowestIndex = indexes[0] - 1;
            const afterHighestIndex = indexes[indexes.length - 1] + 1;

            SeriesService.addIndexIfExists(beforeLowestIndex, serie.data, indexes);
            SeriesService.addIndexIfExists(afterHighestIndex, serie.data, indexes);

            indexes.forEach(index => {
                const point = serie.data[index];
                if (point.y > maxValue) {
                    maxValue = point.y;
                }
            });
        });

        return maxValue;
    }

    private static addIndexIfExists(index: number, arrayToCheck: SeriePoint[], arrayToAdd: number[]) {
        if (arrayToCheck[index] !== undefined) {
            arrayToAdd.push(index);
        }
    }
}
