import { TooltipState } from '../models/tooltip-state.model';
import { MousePosition } from '../models/mouse-position.model';
import { Chart } from '../chart';
import { CircleRenderer } from '../../engine/renderers/circle/circle-renderer';
import { Size } from '../../engine/models/size.model';
import { LineRenderer } from '../../engine/renderers/line/line-renderer';
import { Theme } from '../models/theme.model';

export class HiddenState implements TooltipState {

    private main = this.chart.getMainAxises();

    constructor(private chart: Chart) {}

    onMouseMove(mousePosition: MousePosition) {
        if (this.isOverChart(mousePosition)) {
            this.chart.setTooltipState(this.chart.getTooltipStates().shown);
        }
    }

    draw(_renderer: CircleRenderer, _lineRenderer: LineRenderer, _resolution: Size) {}

    start(_options: any) {}

    stop() {}

    setTheme(_theme: Theme) {}

    private isOverChart(mousePosition: MousePosition) {
        return mousePosition.mouseX >= this.main.axises.xAxis.scale.min
            && mousePosition.mouseX <= this.main.axises.xAxis.scale.max
            && mousePosition.mouseY >= this.main.axises.yAxis.scale.min
            && mousePosition.mouseY <= this.main.axises.yAxis.scale.max;
    }
}
