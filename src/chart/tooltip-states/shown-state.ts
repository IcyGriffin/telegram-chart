import { TooltipState } from '../models/tooltip-state.model';
import { MousePosition } from '../models/mouse-position.model';
import { Chart } from '../chart';
import { CircleRenderer } from '../../engine/renderers/circle/circle-renderer';
import { Size } from '../../engine/models/size.model';
import { SeriePoint } from '../models/serie-model';
import { HelperService } from '../helper.service';
import { ColorService } from '../color.service';
import { LineRenderer } from '../../engine/renderers/line/line-renderer';
import { TransparentColor } from '../../engine/models/transparent-color.model';
import { DateFormattingService } from '../date-formatting.service';
import { Theme } from '../models/theme.model';

export class ShownState implements TooltipState {

    private main = this.chart.getMainAxises();
    private closestPoints: SeriePoint[] = [];
    private prevClosestPoint: SeriePoint;

    private innerColor: TransparentColor;
    private lineColor: TransparentColor;

    private tooltip: {
        container: HTMLDivElement,
        header: HTMLDivElement,
        values: HTMLDivElement
    };
    private tooltipMargin = 10;

    constructor(private chart: Chart) {}

    onMouseMove(mousePosition: MousePosition) {
        if (!this.isOverChart(mousePosition)) {
            this.chart.setTooltipState(this.chart.getTooltipStates().hidden);
            return;
        }

        this.closestPoints = this.getClosestPoints(mousePosition);

        this.chart.requestRedraw();
    }

    draw(circleRenderer: CircleRenderer, lineRenderer: LineRenderer, resolution: Size) {

        if (this.closestPoints.length) {
            this.drawLine(lineRenderer, resolution);
            this.drawCircles(circleRenderer, resolution);
            this.drawTooltip();
        }
    }

    start(options: any) {
        this.tooltip = this.chart.getTooltipMarkup();
    }

    stop() {
        this.closestPoints = [];
        this.prevClosestPoint = null;
        this.tooltip.container.style.visibility = 'hidden';

        this.chart.requestRedraw();
    }

    setTheme(theme: Theme) {
        this.innerColor = {
            ...ColorService.hexToRgb(theme.primaryColor),
            a: 1
        };

        this.lineColor = {
            ...ColorService.hexToRgb(theme.tooltipLineColor),
            a: 1
        };
    }

    private isOverChart(mousePosition: MousePosition) {
        return mousePosition.mouseX >= this.main.axises.xAxis.scale.min
            && mousePosition.mouseX <= this.main.axises.xAxis.scale.max
            && mousePosition.mouseY >= this.main.axises.yAxis.scale.min
            && mousePosition.mouseY <= this.main.axises.yAxis.scale.max;
    }

    private getClosestPoints(mousePosition: MousePosition): SeriePoint[] {
        const currentDomainValue = HelperService.mapScaleToDomain(
            mousePosition.mouseX,
            this.main.axises.xAxis.domain,
            this.main.axises.xAxis.scale
        );

        let closestPoints: SeriePoint[] = this.chart.getSeries().map(serie => serie.data[0]);
        this.chart.getSeries().forEach((serie, index) => {
            serie.data.forEach(point => {
                if (Math.abs(point.x.getTime() - currentDomainValue) < Math.abs(closestPoints[index].x.getTime() - currentDomainValue)) {
                    closestPoints[index] = point;
                }
            });
        });

        return closestPoints;
    }

    private drawLine(lineRenderer: LineRenderer, resolution: Size) {
        const closestPoint = this.closestPoints[0];
        lineRenderer.draw({
            lines: [{
                from: {
                    x: HelperService.mapDomainToScale(
                        closestPoint.x.getTime(),
                        this.main.axises.xAxis.domain,
                        this.main.axises.xAxis.scale
                    ),
                    y: this.main.axises.yAxis.scale.min
                },
                to: {
                    x: HelperService.mapDomainToScale(
                        closestPoint.x.getTime(),
                        this.main.axises.xAxis.domain,
                        this.main.axises.xAxis.scale
                    ),
                    y: this.main.axises.yAxis.scale.max
                },
                color: this.lineColor
            }],
            resolution: resolution
        });
    }

    private drawCircles(circleRenderer: CircleRenderer, resolution: Size) {
        const series = this.chart.getSeries();
        const circles = this.closestPoints.map((closestPoint, index) => {
            return {
                center: {
                    x: HelperService.mapDomainToScale(
                        closestPoint.x.getTime(),
                        this.main.axises.xAxis.domain,
                        this.main.axises.xAxis.scale
                    ),
                    y: HelperService.mapDomainToScale(
                        closestPoint.y,
                        this.main.axises.yAxis.domain,
                        this.main.axises.yAxis.scale
                    ),
                },
                color: {
                    ...ColorService.hexToRgb(series[index].color),
                    a: 1
                },
                innerColor: this.innerColor,
                radius: 5
            };
        });

        circleRenderer.draw({
            circles: circles,
            resolution: resolution
        });
    }

    private drawTooltip() {
        const series = this.chart.getSeries();
        const closestPoint = this.closestPoints[0];

        const isEqual = this.prevClosestPoint
            && this.prevClosestPoint.x === closestPoint.x
            && this.prevClosestPoint.y === closestPoint.y;

        if (isEqual) {
            return;
        }

        this.prevClosestPoint = closestPoint;

        this.tooltip.header.textContent = DateFormattingService.format(closestPoint.x);

        while (this.tooltip.values.hasChildNodes()) {
            this.tooltip.values.removeChild(this.tooltip.values.firstChild);
        }

        this.closestPoints.forEach((point, index) => {
            const element = document.createElement('div');
            element.style.padding = '10px';
            element.style.color = series[index].color;

            const numbersElement = document.createElement('div');
            numbersElement.textContent = `${ point.y }`;

            const textElement = document.createElement('div');
            textElement.textContent = series[index].name;

            element.appendChild(numbersElement);
            element.appendChild(textElement);

            this.tooltip.values.appendChild(element);
        });

        const width = this.tooltip.container.clientWidth;
        const height = this.tooltip.container.clientHeight;

        let xPosition = HelperService.mapDomainToScale(
            closestPoint.x.getTime(),
            this.main.axises.xAxis.domain,
            this.main.axises.xAxis.scale
        ) - width / 2;
        const yPosition = this.main.axises.yAxis.scale.max;

        if (this.isHiddenUnderTooltip(yPosition, height)) {
            let newPosition = xPosition - (width / 2) - this.tooltipMargin;
            if (newPosition < 0) {
                newPosition = xPosition + (width / 2) + this.tooltipMargin;
            }

            xPosition = newPosition;
        }

        if (xPosition < 0) {
            xPosition = 0;
        }

        if (xPosition >= this.main.axises.xAxis.scale.max - width) {
            xPosition = this.main.axises.xAxis.scale.max - width - this.tooltipMargin;
        }

        this.tooltip.container.style.transform = `translate(${ xPosition }px, -${ yPosition }px)`;
        this.tooltip.container.style.visibility = 'visible';
    }

    private isHiddenUnderTooltip(yPosition: number, height: number): boolean {
        return this.closestPoints.some((point) => {
            const pointY = HelperService.mapDomainToScale(
                point.y,
                this.main.axises.yAxis.domain,
                this.main.axises.yAxis.scale
            );

            return pointY >= yPosition - height;
        });
    }
}
