import { Size } from './models/size.model';
import { Color } from './models/color.model';
import * as vertexShaderSource from './renderers/shaders/vertex.shader';
import * as fragmentShaderSource from './renderers/shaders/fragment.shader';

export class Engine {

    private gl: WebGLRenderingContext;
    private defaultProgram: WebGLProgram;
    private programInUse: WebGLProgram;

    constructor(context: WebGLRenderingContext) {
        this.gl = context;
        this.gl.blendFunc(this.gl.SRC_ALPHA, this.gl.ONE_MINUS_SRC_ALPHA);
        this.gl.enable(this.gl.BLEND);
        this.gl.disable(this.gl.DEPTH_TEST);

        this.defaultProgram = this.initProgram();
    }

    setSize(size: Size) {
        this.gl.viewport(0, 0, size.width, size.height);
    }

    clear(color: Color) {
        this.gl.clearColor(color.r, color.g, color.b, 1.0);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);
    }

    createShader(type: GLenum, code: string): WebGLShader {
        const shader = this.gl.createShader(type);
        this.gl.shaderSource(shader, code);
        this.gl.compileShader(shader);
        return shader;
    }

    createProgram(shaders: WebGLShader[]): WebGLProgram {
        const program = this.gl.createProgram();
        shaders.forEach(shader => {
            this.gl.attachShader(program, shader);
        });

        this.gl.linkProgram(program);
        return program;
    }

    getAttribute(program: WebGLProgram, name: string): GLint {
        const attribute = this.gl.getAttribLocation(program, name);
        this.gl.enableVertexAttribArray(attribute);
        return attribute;
    }

    getUniform(program: WebGLProgram, name: string): WebGLUniformLocation {
        return this.gl.getUniformLocation(program, name);
    }

    createBuffer(): WebGLBuffer {
        return this.gl.createBuffer();
    }

    prepareAttributeData(attribute: GLint, size: number) {
        this.gl.vertexAttribPointer(attribute, size, this.gl.FLOAT, false, 0, 0);
    }

    useProgram(program: WebGLProgram) {
        if (this.programInUse === program) {
            return;
        }

        this.programInUse = program;
        this.gl.useProgram(program);
    }

    getDefaultProgram(): WebGLProgram {
        return this.defaultProgram;
    }

    private initProgram(): WebGLProgram {
        const vertexShader = this.createShader(this.gl.VERTEX_SHADER, vertexShaderSource);
        const fragmentShader = this.createShader(this.gl.FRAGMENT_SHADER, fragmentShaderSource);
        return this.createProgram([vertexShader, fragmentShader]);
    }
}
