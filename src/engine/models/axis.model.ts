import { Limit } from './limit.model';

export interface Axis {
    domain: Limit;
    scale: Limit;
}
