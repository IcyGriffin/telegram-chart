import { Point } from './point.model';
import { TransparentColor } from './transparent-color.model';

export interface Circle {
    center: Point;
    radius: number;
    color: TransparentColor;
    innerColor: TransparentColor;
}
