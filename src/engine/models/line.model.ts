import { Point } from './point.model';
import { TransparentColor } from './transparent-color.model';

export interface Line {
    from: Point;
    to: Point;
    color: TransparentColor;
}
