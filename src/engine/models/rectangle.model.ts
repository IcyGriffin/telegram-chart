import { Point } from './point.model';
import { Size } from './size.model';
import { TransparentColor } from './transparent-color.model';

export interface Rectangle {
    from: Point;
    size: Size;
    color: TransparentColor;
}
