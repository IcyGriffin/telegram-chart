import { Point } from './point.model';
import { TransparentColor } from './transparent-color.model';

export interface StripLine {
    id: string;
    points: Point[];
    color: TransparentColor;
}
