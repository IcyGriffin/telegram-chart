import { Color } from './color.model';

export interface TransparentColor extends Color {
    a: number;
}
