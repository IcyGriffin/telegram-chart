import { Coordinates } from '../models/coordinates.model';
import { Circle } from '../models/circle.model';

export class CircleService {

    static getCoordinates(circles: Circle[]): Coordinates {
        return circles.reduce<Coordinates>((result, circle) => {
            result = [].concat(result, this.createCircle(circle));
            return result;
        }, []);
    }

    private static createCircle(circle: Circle): number[] {
        const { center, radius } = circle;
        return [
            center.x - radius, center.y - radius, // bottom left
            center.x - radius, center.y + radius, // top left
            center.x + radius, center.y + radius, // top right
            center.x + radius, center.y + radius, // top right
            center.x + radius, center.y - radius, // bottom right
            center.x - radius, center.y - radius, // bottom left
        ];
    }
}
