import { Line } from '../models/line.model';
import { Coordinates } from '../models/coordinates.model';

export class LineService {
    static getCoordinates(lines: Line[]): Coordinates {
        return lines.reduce<Coordinates>((result, line) => {
            result.push(line.from.x);
            result.push(line.from.y);
            result.push(line.to.x);
            result.push(line.to.y);
            return result;
        }, []);
    }
}
