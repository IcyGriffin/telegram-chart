import { Coordinates } from '../models/coordinates.model';
import { Rectangle } from '../models/rectangle.model';

export class RectangleService {

    static getCoordinates(rectangle: Rectangle): Coordinates {
        const { from, size } = rectangle;
        return [
            from.x, from.y,
            from.x, from.y + size.height,
            from.x + size.width, from.y + size.height,
            from.x + size.width, from.y
        ];
    }
}
