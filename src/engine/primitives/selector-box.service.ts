import { Coordinates } from '../models/coordinates.model';
import { Rectangle } from '../models/rectangle.model';
import { Colors } from '../models/colors.model';

export class SelectorBoxService {

    private static pointsPerSide = 6;
    private static pointsPerSelectorBox = SelectorBoxService.pointsPerSide * 4;

    private static borderWidth = {
        primary: 5,
        secondary: 2
    };

    static getCoordinates(rectangle: Rectangle): Coordinates {
        return [].concat(
            this.leftBorder(rectangle),
            this.topBorder(rectangle),
            this.rightBorder(rectangle),
            this.bottomBorder(rectangle)
        );
    }

    private static leftBorder(rectangle: Rectangle): number[] {
        const { from, size } = rectangle;
        return [
            // left triangle
            from.x, from.y,
            from.x, from.y + size.height,
            from.x + SelectorBoxService.borderWidth.primary, from.y + size.height,
            // right triangle
            from.x + SelectorBoxService.borderWidth.primary, from.y + size.height,
            from.x + SelectorBoxService.borderWidth.primary, from.y,
            from.x, from.y,
        ];
    }

    private static topBorder(rectangle: Rectangle): number[] {
        const { from, size } = rectangle;
        return [
            // top triangle
            from.x, from.y + size.height,
            from.x + size.width, from.y + size.height,
            from.x + size.width, from.y + size.height - SelectorBoxService.borderWidth.secondary,
            // bottom triangle
            from.x + size.width, from.y + size.height - SelectorBoxService.borderWidth.secondary,
            from.x, from.y + size.height - SelectorBoxService.borderWidth.secondary,
            from.x, from.y + size.height,
        ];
    }

    private static rightBorder(rectangle: Rectangle): number[] {
        const { from, size } = rectangle;
        return [
            // left triangle
            from.x + size.width - SelectorBoxService.borderWidth.primary, from.y,
            from.x + size.width - SelectorBoxService.borderWidth.primary, from.y + size.height,
            from.x + size.width, from.y + size.height,
            // bottom triangle
            from.x + size.width - SelectorBoxService.borderWidth.primary, from.y,
            from.x + size.width, from.y + size.height,
            from.x + size.width, from.y,
        ];
    }

    private static bottomBorder(rectangle: Rectangle): number[] {
        const { from, size } = rectangle;
        return [
            // top triangle
            from.x, from.y,
            from.x, from.y + SelectorBoxService.borderWidth.secondary,
            from.x + size.width, from.y + SelectorBoxService.borderWidth.secondary,
            // bottom triangle
            from.x, from.y,
            from.x + size.width, from.y + SelectorBoxService.borderWidth.secondary,
            from.x + size.width, from.y,
        ];
    }
}
