import { StripLine } from '../models/strip-line.model';
import { Coordinates } from '../models/coordinates.model';

export class StripLineService {
    static getCoordinates(line: StripLine): Coordinates {
        return line.points.reduce<Coordinates>((result, point) => {
            result.push(point.x);
            result.push(point.y);
            return result;
        }, []);
    }
}