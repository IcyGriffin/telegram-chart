import { Engine } from '../../engine';
import * as vertexShaderSource from './shaders/vertex.shader';
import * as fragmentShaderSource from './shaders/fragment.shader';
import { DrawOptions } from './draw-options.model';
import { Size } from '../../models/size.model';
import { LineService } from '../../primitives/line.service';
import { CircleService } from '../../primitives/circle.service';
import { TransparentColor } from '../../models/transparent-color.model';
import { Point } from '../../models/point.model';

export class CircleRenderer {
    private gl: WebGLRenderingContext;
    private engine: Engine;

    private program: WebGLProgram;

    private coordinateBuffer: WebGLBuffer;

    private coordinateAttribute: GLint;

    private resolutionUniform: WebGLUniformLocation;
    private innerColorUniform: WebGLUniformLocation;
    private strokeColorUniform: WebGLUniformLocation;
    private centerUniform: WebGLUniformLocation;

    private radiusUniform: WebGLUniformLocation;
    private strokeUniform: WebGLUniformLocation;

    private readonly coordPerVertexShader = 2; // x, y coordinates
    private readonly colorsPerVertexShader = 4; // r, g, b, a colors

    constructor(gl: WebGLRenderingContext, engine: Engine) {
        this.gl = gl;
        this.engine = engine;

        this.init();
    }

    draw(drawOptions: DrawOptions) {
        this.engine.useProgram(this.program);

        this.setResolution(drawOptions.resolution);
        drawOptions.circles.forEach(circle => {
            this.setInnerColor(circle.innerColor);
            this.setStrokeColor(circle.color);
            this.setCenter(circle.center);
            this.setRadius(circle.radius);
            this.setStroke(2);

            const coordinates = CircleService.getCoordinates(drawOptions.circles);
            this.setCoordinates(coordinates);

            const numberOfPoints = coordinates.length / this.coordPerVertexShader;
            this.gl.drawArrays(this.gl.TRIANGLES, 0, numberOfPoints);
        });

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null);
    }

    private init() {
        this.initProgram();
        this.initBuffers();
        this.initAttributes();
        this.initUniforms();
    }

    private initProgram() {
        const vertexShader = this.engine.createShader(this.gl.VERTEX_SHADER, vertexShaderSource);
        const fragmentShader = this.engine.createShader(this.gl.FRAGMENT_SHADER, fragmentShaderSource);
        this.program = this.engine.createProgram([vertexShader, fragmentShader]);
    }

    private initBuffers() {
        this.coordinateBuffer = this.engine.createBuffer();
    }

    private initAttributes() {
        this.coordinateAttribute = this.engine.getAttribute(this.program, 'coordinates');
    }

    private initUniforms() {
        this.resolutionUniform = this.engine.getUniform(this.program, 'resolution');
        this.innerColorUniform = this.engine.getUniform(this.program, 'innerColor');
        this.strokeColorUniform = this.engine.getUniform(this.program, 'strokeColor');
        this.centerUniform = this.engine.getUniform(this.program, 'center');
        this.radiusUniform = this.engine.getUniform(this.program, 'radius');
        this.strokeUniform = this.engine.getUniform(this.program, 'stroke');
    }

    private setResolution(resolution: Size) {
        this.gl.uniform2f(this.resolutionUniform, resolution.width, resolution.height);
    }

    private setInnerColor(color: TransparentColor) {
        this.gl.uniform4f(this.innerColorUniform, color.r, color.g, color.b, color.a);
    }

    private setStrokeColor(color: TransparentColor) {
        this.gl.uniform4f(this.strokeColorUniform, color.r, color.g, color.b, color.a);
    }

    private setCenter(point: Point) {
        this.gl.uniform2f(this.centerUniform, point.x, point.y);
    }

    private setRadius(radius: number) {
        this.gl.uniform1f(this.radiusUniform, radius);
    }

    private setStroke(stroke: number) {
        this.gl.uniform1f(this.strokeUniform, stroke);
    }

    private setCoordinates(coordinates: number[]) {
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.coordinateBuffer);
        this.engine.prepareAttributeData(this.coordinateAttribute, this.coordPerVertexShader);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(coordinates), this.gl.STATIC_DRAW);
    }
}