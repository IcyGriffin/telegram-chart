import { Size } from '../../models/size.model';
import { Circle } from '../../models/circle.model';

export interface DrawOptions {
    circles: Circle[];
    resolution: Size;
}