precision mediump float;

uniform vec4 innerColor;
uniform vec4 strokeColor;
uniform vec2 center;
// uniform vec2 resolution;

uniform float radius;
uniform float stroke;

varying vec2 vPosition;
varying vec2 vResolution;

vec2 pixelToPosition(vec2 pixel) {
    vec2 zeroToOne = pixel.xy / vResolution;
    vec2 zeroToTwo = zeroToOne * 2.0;
    vec2 clipSpace = zeroToTwo - 1.0;
    return clipSpace;
}

void main(void) {
    float d = distance(vPosition, center);
    float innerRadius = radius - stroke;
    if (d <= innerRadius) gl_FragColor = innerColor;
    else if(d <= radius && d > innerRadius) gl_FragColor = strokeColor;
    else discard;
}
