attribute vec2 coordinates;

uniform vec2 resolution;

varying lowp vec2 vPosition;
varying lowp vec2 vResolution;

vec2 pixelToPosition(vec2 pixel) {
    vec2 zeroToOne = pixel.xy / resolution;
    vec2 zeroToTwo = zeroToOne * 2.0;
    vec2 clipSpace = zeroToTwo - 1.0;
    return clipSpace;
}

void main(void) {
    vec2 position = pixelToPosition(coordinates);
    
    gl_Position = vec4(position, 0.0, 1.0);
    vPosition = coordinates;
    vResolution = resolution;
}
