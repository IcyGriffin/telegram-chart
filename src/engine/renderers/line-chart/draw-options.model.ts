import { StripLine } from '../../models/strip-line.model';
import { Axis } from '../../models/axis.model';
import { Size } from '../../models/size.model';

export interface DrawOptions {
    line: StripLine;
    axises: {
        x: Axis,
        y: Axis
    };
    resolution: Size;
}