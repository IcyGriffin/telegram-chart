import { Engine } from '../../engine';
import { StripLineService } from '../../primitives/strip-line.service';
import { DrawOptions } from './draw-options.model';
import { Size } from '../../models/size.model';
import { TransparentColor } from '../../models/transparent-color.model';

export class LineChartRenderer {
    private gl: WebGLRenderingContext;
    private engine: Engine;

    private program: WebGLProgram;

    private coordinateBuffer: WebGLBuffer;

    private coordinateAttribute: GLint;

    private resolutionUniform: WebGLUniformLocation;
    private colorUniform: WebGLUniformLocation;

    private readonly coordPerVertexShader = 2; // x, y coordinates

    constructor(gl: WebGLRenderingContext, engine: Engine) {
        this.gl = gl;
        this.engine = engine;

        this.init();
    }

    draw(drawOptions: DrawOptions) {
        this.engine.useProgram(this.program);

        this.setResolution(drawOptions.resolution);

        const coordinates = StripLineService.getCoordinates(drawOptions.line);
        this.setCoordinates(coordinates);

        this.setColors(drawOptions.line.color);

        const numberOfPoints = coordinates.length / this.coordPerVertexShader;
        this.gl.drawArrays(this.gl.LINE_STRIP, 0, numberOfPoints);
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null);
    }

    private init() {
        this.initProgram();
        this.initBuffers();
        this.initAttributes();
        this.initUniforms();
    }

    private initProgram() {
        this.program = this.engine.getDefaultProgram();
    }

    private initBuffers() {
        this.coordinateBuffer = this.engine.createBuffer();
    }

    private initAttributes() {
        this.coordinateAttribute = this.engine.getAttribute(this.program, 'coordinates');
    }

    private initUniforms() {
        this.resolutionUniform = this.engine.getUniform(this.program, 'resolution');
        this.colorUniform = this.engine.getUniform(this.program, 'colors');
    }

    private setResolution(resolution: Size) {
        this.gl.uniform2f(this.resolutionUniform, resolution.width, resolution.height);
    }

    private setCoordinates(coordinates: number[]) {
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.coordinateBuffer);
        this.engine.prepareAttributeData(this.coordinateAttribute, this.coordPerVertexShader);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(coordinates), this.gl.STATIC_DRAW);
    }

    private setColors(color: TransparentColor) {
        this.gl.uniform4f(this.colorUniform, color.r, color.g, color.b, color.a);
    }
}