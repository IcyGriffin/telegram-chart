import { Size } from '../../models/size.model';
import { Line } from '../../models/line.model';

export interface DrawOptions {
    lines: Line[];
    resolution: Size;
}