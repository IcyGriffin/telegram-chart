import { Size } from '../../models/size.model';
import { Rectangle } from '../../models/rectangle.model';

export interface DrawOptions {
    rectangle: Rectangle;
    resolution: Size;
}
