import { Engine } from '../../engine';
import { DrawOptions } from './draw-options.model';
import { Size } from '../../models/size.model';
import { SelectorBoxService } from '../../primitives/selector-box.service';
import { TransparentColor } from '../../models/transparent-color.model';

export class SelectorBoxRenderer {

    private gl: WebGLRenderingContext;
    private engine: Engine;

    private program: WebGLProgram;

    private coordinateBuffer: WebGLBuffer;

    private coordinateAttribute: GLint;

    private resolutionUniform: WebGLUniformLocation;
    private colorUniform: WebGLUniformLocation;

    private readonly coordPerVertexShader = 2; // x, y coordinates

    constructor(gl: WebGLRenderingContext, engine: Engine) {
        this.gl = gl;
        this.engine = engine;

        this.init();
    }

    draw(drawOptions: DrawOptions) {
        this.engine.useProgram(this.program);

        this.setResolution(drawOptions.resolution);

        const coordinates = SelectorBoxService.getCoordinates(drawOptions.rectangle);
        this.setCoordinates(coordinates);

        this.setColors(drawOptions.rectangle.color);

        const numberOfPoints = coordinates.length / this.coordPerVertexShader;
        this.gl.drawArrays(this.gl.TRIANGLES, 0, numberOfPoints);
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null);
    }

    private init() {
        this.initProgram();
        this.initBuffers();
        this.initAttributes();
        this.initUniforms();
    }

    private initProgram() {
        this.program = this.engine.getDefaultProgram();
    }

    private initBuffers() {
        this.coordinateBuffer = this.engine.createBuffer();
    }

    private initAttributes() {
        this.coordinateAttribute = this.engine.getAttribute(this.program, 'coordinates');
    }

    private initUniforms() {
        this.resolutionUniform = this.engine.getUniform(this.program, 'resolution');
        this.colorUniform = this.engine.getUniform(this.program, 'colors');
    }

    private setResolution(resolution: Size) {
        this.gl.uniform2f(this.resolutionUniform, resolution.width, resolution.height);
    }

    private setCoordinates(coordinates: number[]) {
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.coordinateBuffer);
        this.engine.prepareAttributeData(this.coordinateAttribute, this.coordPerVertexShader);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(coordinates), this.gl.STATIC_DRAW);
    }

    private setColors(color: TransparentColor) {
        this.gl.uniform4f(this.colorUniform, color.r, color.g, color.b, color.a);
    }
}
