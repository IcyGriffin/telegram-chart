precision mediump float;

uniform vec4 colors;

void main(void) {
    gl_FragColor = colors;
}
