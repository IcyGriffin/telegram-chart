import { ChartData } from './chart/models/chart-data.model';

type onChangeCallback = (ids: string[]) => void;
type onInputChangeCallback = (item: Item, isChecked: boolean) => void;
interface SelectedIdsMap {
    [key: string]: boolean;
}

interface Item {
    label: string;
    id: string;
    color: string;
}

export class LegendService {
    static createLegend(container: HTMLElement, chartData: ChartData, onChangeCallback: onChangeCallback) {
        const items = LegendService.getItems(chartData);
        const selectedIdsMap = LegendService.createSelectedIdsMap(items);
        const listener = (item: Item, isChecked: boolean) => {
            selectedIdsMap[item.id] = isChecked;
            const selectedIds = Object.keys(selectedIdsMap)
                .filter(itemId => selectedIdsMap[itemId]);

            onChangeCallback(selectedIds);
            LegendService.setDisabledState(container, selectedIds);
        };

        items.forEach(item => {
            const element = LegendService.createItemElement(item, listener);
            container.appendChild(element);
        });
    }

    private static getItems(chartData: ChartData): Item[] {
        const { types, names, colors } = chartData;
        return Object.keys(chartData.types)
            .filter(id => types[id] === 'line')
            .map(id => {
                return {
                    id: id,
                    label: names[id],
                    color: colors[id]
                };
            });
    }

    private static createItemElement(item: Item, listener: onInputChangeCallback): HTMLElement {
        const itemContainer = document.createElement('div');
        itemContainer.className = 'item-element';
        const label = LegendService.createLabel(item, listener);
        itemContainer.appendChild(label);

        return itemContainer;
    }

    private static createLabel(item: Item, listener: onInputChangeCallback): HTMLLabelElement {
        const label = document.createElement('label');
        label.className = 'item-selectable selected';
        label.style.color = item.color;

        const input = LegendService.createInput(item, listener);
        const text = LegendService.createText(item.label);
        label.appendChild(input);
        label.appendChild(text);

        return label;
    }

    private static createInput(item: Item, listener: onInputChangeCallback): HTMLInputElement {
        const input = document.createElement('input');
        input.className = 'item-selectable';
        input.type = 'checkbox';
        input.checked = true;

        input.addEventListener('change', () => {
            listener(item, input.checked);
            const parentLabel = input.parentElement;
            parentLabel.classList.toggle('selected');
        });

        return input;
    }

    private static createText(text: string): Text {
        const textElement = document.createTextNode('input');
        textElement.textContent = text;

        return textElement;
    }

    private static createSelectedIdsMap(items: Item[]): SelectedIdsMap {
        return items.reduce<SelectedIdsMap>((result, item) => {
            result[item.id] = true;
            return result;
        }, {});
    }

    private static setDisabledState(container: HTMLElement, selectedIds: string[]) {
        const shouldDisableSelected = selectedIds.length === 1;
        const collection = container.getElementsByTagName('input');
        for (let index = 0; index < collection.length; index++) {
            const element = collection[index];
            if (element.checked) {
                element.disabled = shouldDisableSelected;
                if (shouldDisableSelected) {
                    element.parentElement.classList.remove('item-selectable');
                    element.classList.remove('item-selectable');
                } else {
                    element.parentElement.classList.add('item-selectable');
                    element.classList.add('item-selectable');
                }
            }
        }
    }
}
