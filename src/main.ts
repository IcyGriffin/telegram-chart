import { Chart } from './chart/chart';
import * as chartData from '../static/chart_data.json';
import { LegendService } from './legend.service';
import { light } from './themes';
import { Theme } from './chart/models/theme.model';
import { ThemeService } from './theme.service';
import { ChartData } from './chart/models/chart-data.model';
import { createContainer, createChartTitle, createChartContainer, createChartLegend } from './chart/markup-helpers';
import './styles.css';

const chartStorage: Chart[] = [];

const charts = document.getElementById('charts');
chartData.forEach((currentChartData: ChartData, index: string) => {
    const container = createContainer();
    const chartTitle = createChartTitle(`Chart ${ index }`);
    const chartContainer = createChartContainer();
    const chartLegend = createChartLegend();

    container.appendChild(chartTitle);
    container.appendChild(chartContainer);
    container.appendChild(chartLegend);
    charts.appendChild(container);

    const chart = new Chart(chartContainer, currentChartData);
    chart.setTheme(light);

    LegendService.createLegend(chartLegend, currentChartData, (selectedIds: string[]) => {
        chart.showSeries(selectedIds);
    });

    chartStorage.push(chart);
});

ThemeService.onChange((theme: Theme) => {
    chartStorage.forEach(chart => {
        chart.setTheme(theme);
    });
});
