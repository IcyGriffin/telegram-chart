import { dark, light } from './themes';
import { Theme } from './chart/models/theme.model';

type Callback = (theme: Theme) => void;

export class ThemeService {
    private static themes = {
        dark: dark,
        light: light
    };

    private static currentTheme = ThemeService.themes.light;

    private static switchMode = document.getElementById('switch-theme');

    static onChange(callback: Callback) {
        ThemeService.setPageTheme(light);

        ThemeService.switchMode.addEventListener('click', () => {
            ThemeService.currentTheme = ThemeService.currentTheme === ThemeService.themes.light
                ? ThemeService.themes.dark
                : ThemeService.themes.light;

            ThemeService.setPageTheme(ThemeService.currentTheme);
            callback(ThemeService.currentTheme);
        });
    }

    private static setPageTheme(theme: Theme) {
        document.body.style.backgroundColor = theme.primaryColor;
        document.body.style.color = theme.contrastColor;
        ThemeService.switchMode.style.color = theme.contrastColor;

        const switchToTheme = theme === ThemeService.themes.dark
            ? 'Light'
            : 'Dark';

        document.body.className = theme === ThemeService.themes.dark
            ? 'dark'
            : 'light';

        const text = `Switch to ${ switchToTheme } Mode`;
        ThemeService.switchMode.textContent = text;
    }
}
