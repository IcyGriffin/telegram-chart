import { Theme } from './chart/models/theme.model';

export const dark: Theme = {
    primaryColor: '#242f3e',
    contrastColor: '#ffffff',
    tooltipBorderColor: '#202b38',
    tooltipLineColor: '#3b4a5a',
    markColor: '#536576',
    yAxisLineColor: '#293544',
    selectorBoxColor: '#40566b',
    selectorHiderColor: '#1f2a38',
};

export const light: Theme = {
    primaryColor: '#ffffff',
    contrastColor: '#000000',
    tooltipBorderColor: '#a2adb3',
    tooltipLineColor: '#dfe6eb',
    markColor: '#a2adb3',
    yAxisLineColor: '#f2f4f5',
    selectorBoxColor: '#ddeaf3',
    selectorHiderColor: '#f5f9fb',
};